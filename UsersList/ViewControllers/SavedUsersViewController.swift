//
//  SavedUsersViewController.swift
//  UsersList
//
//  Created by mac on 08.03.2021.
//

import UIKit

protocol SavedUsersViewControllerOutput {
    var info: [UserInfo] { get set }
    func loadData()
}

class SavedUsersViewController: UIViewController {
    
    @IBOutlet weak var savedUsersTableView: UITableView!
    
    lazy var output: SavedUsersViewControllerOutput = {
        let presenter = SavedUsersPresenter()
        presenter.output = self
        return presenter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        output.loadData()
    }
    
    func deleteData(indexPath:IndexPath) {
        output.info.remove(at: indexPath.row)
    }
    
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension SavedUsersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.info.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let info = output.info[indexPath.row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? HomePageTableViewCell {
            
            var image: UIImage?
            guard let firstNameOp = info.firstName, let lastNameOp = info.lastName else {return UITableViewCell()}
            
            if let data = info.photo,
               let loadedImage = try?  NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [Data] {
                let images = loadedImage.compactMap({ UIImage(data: $0) })
                image = images.first
                cell.photoImageView.image = image
                cell.setup(firstName: firstNameOp, lastName: lastNameOp, phone: info.phone ?? "")
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let controller = storyboard?.instantiateViewController(identifier: "EditUserViewController") as? EditUserViewController else {return}
        let usrInfo = Result(userCoreData: output.info[indexPath.row])
        controller.users = usrInfo
        
        navigationController?.pushViewController(controller, animated: true)
    
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .normal, title: "delete") { [weak self] (_, _, _)  in
            
            self?.savedUsersTableView.setEditing(true, animated: true)
            
            if let info = self?.output.info[indexPath.row] {
                CoreDataManager.shared.viewContext.delete(info)
            }
            
            CoreDataManager.shared.saveContext()
            self?.deleteData(indexPath: indexPath)
            
        }
        let action = UISwipeActionsConfiguration(actions: [delete])
        delete.backgroundColor = #colorLiteral(red: 1, green: 0.2211629322, blue: 0.03118017327, alpha: 1)
        return action
    }
    
}

// MARK: - SavedUsersPresenterOutput
extension SavedUsersViewController: SavedUsersPresenterOutput {
    
    func didDataUpdate() {
        savedUsersTableView.reloadData()
    }
}
