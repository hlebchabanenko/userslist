//
//  EditUserViewController.swift
//  UsersList
//
//  Created by mac on 06.03.2021.
//

import UIKit
import CoreData

class EditUserViewController: UIViewController {
    
    @IBOutlet weak var avatarUserImage: UIImageView! {
        didSet {
            let photo = try? users?.picture.medium.asURL()
            avatarUserImage.setImageWith(url: photo)
            avatarUserImage.layer.cornerRadius = avatarUserImage.frame.height / 2
            avatarUserImage.clipsToBounds = true
        }
    }
    var onDataAdded: ((UserInfo) -> Void)?
    var userInfo: UserInfo?
    var imagePicker = ImagePicker()
    var users: Result?
    var didTestFieldChange: ((String) -> Void)?
    
    var firstName: String?
    var userMobile: String?
    var secondName: String?
    var email: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Edit User Profile"
        self.hideKeyboardWhenTappedaround()
    }
    
    @IBAction func saveUsers(_ sender: Any) {
        
        let photo1 = NSMutableArray()
        var imageData = Data()
        let image = avatarUserImage.image
        let data : NSData = NSData(data: (image?.pngData())!)
        photo1.add(data)
        guard let photoObject = try? NSKeyedArchiver.archivedData(withRootObject: photo1, requiringSecureCoding: false) else {return}
        imageData = photoObject
        
        guard let firstlNameOp  = users?.name.first, let secondNameOp = users?.name.last, let phoneOp = users?.phone, let emailOp = users?.email else {return}
        let addInfo = self.saveUsersInfo(firstName:   firstName ?? firstlNameOp, lastName: secondName ?? secondNameOp, phone: (userMobile ?? phoneOp) , photo: imageData, email: email ?? emailOp )
        onDataAdded?(addInfo)
        
        tabBarController?.selectedIndex = 1
        let navigation  = tabBarController?.selectedViewController?.navigationController
        
        guard let savedAddress = self.storyboard?.instantiateViewController(withIdentifier: "SavedUsersViewController") as? SavedUsersViewController else {return}
        navigation?.pushViewController(savedAddress, animated: true)
        
    }
    
    func saveUsersInfo(firstName: String, lastName: String, phone: String, photo: Data, email: String) -> UserInfo {
        let entity = NSEntityDescription.entity(forEntityName: "UserInfo", in: CoreDataManager.shared.viewContext)!
        let usersInfo = UserInfo(entity: entity, insertInto: CoreDataManager.shared.viewContext)
        usersInfo.save(firstName: firstName, lastName: lastName, phone: phone, photo: photo, user: usersInfo, email: email)
        CoreDataManager.shared.saveContext()
        return usersInfo
    }
    
    @IBAction func chooseImage(_ sender: Any) {
        
        imagePicker.pickImage(from: self)
        imagePicker.didImagePick = { [weak self] in
            self?.avatarUserImage.image = $0
            
        }
    }
    
    @objc func firstNameTextFieldDidChange(textField: UITextField) {
        firstName = textField.text
        didTestFieldChange?(firstName ?? "")
        
    }
    @objc func secondNameTextFieldDidChangeTwo(textField: UITextField) {
        secondName = textField.text
        didTestFieldChange?(secondName ?? "")
    }
    
    @objc func phoneTextFieldDidChangeTwo(textField: UITextField) {
        userMobile = textField.text
        didTestFieldChange?(userMobile ?? "")
    }
    
    @objc func emailTextFieldDidChangeTwo(textField: UITextField) {
        email = textField.text
        didTestFieldChange?(email ?? "")
    }
    
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension EditUserViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? EditUserTableViewCell {
            
            var key = ""
            var value = ""
            
            switch indexPath.row {
            case 0:
                key = "First name"
                value = users?.name.first ?? ""
                cell.valueTextField.addTarget(self, action: #selector(firstNameTextFieldDidChange(textField:)), for: .editingChanged)
            case 1:
                key = "last name"
                value = users?.name.last ?? ""
                cell.valueTextField.addTarget(self, action: #selector(secondNameTextFieldDidChangeTwo(textField:)), for: .editingChanged)
            case 2:
                key = "Email"
                value = users?.email ?? ""
                cell.valueTextField.addTarget(self, action: #selector(emailTextFieldDidChangeTwo(textField:)), for: .editingChanged)
            case 3:
                key = "Phone"
                value = users?.phone ?? ""
                cell.valueTextField.addTarget(self, action: #selector(phoneTextFieldDidChangeTwo(textField:)), for: .editingChanged)
            default: break
                
            }
            cell.setup(key: key, value: value)
            return cell
        }
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension UserInfo {
    
    func save(firstName:String, lastName: String, phone: String, photo: Data, user: UserInfo, email: String) {
        user.setValue(firstName, forKey: "firstName")
        user.setValue(phone, forKey: "phone")
        user.setValue(photo, forKey: "photo")
        user.setValue(email, forKey: "email")
        user.setValue(lastName, forKey: "lastName")
    }
}
