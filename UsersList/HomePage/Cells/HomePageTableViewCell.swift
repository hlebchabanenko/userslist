//
//  HomePageTableViewCell.swift
//  UsersList
//
//  Created by mac on 06.03.2021.
//

import UIKit

class HomePageTableViewCell: UITableViewCell {

    @IBOutlet weak  var photoImageView: UIImageView! {
        didSet {
            photoImageView.layer.cornerRadius = photoImageView.frame.height / 2
            photoImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var phoneLabel: UILabel!
    
    func setup(firstName: String, lastName: String, phone: String) {
        nameLabel.text = firstName + " " + lastName
        phoneLabel.text = phone
        
    }
}
