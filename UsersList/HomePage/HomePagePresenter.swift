//
//  HomePagePresenter.swift
//  UsersList
//
//  Created by mac on 06.03.2021.
//

import Foundation
import Alamofire
import SwiftyJSON

class Service {
    
    static func downloadJson(completion: @escaping ([Result]) -> Void) {
        guard let url = URL(string: "https://randomuser.me/api/?results=15") else { return  }
        var users: [Result] = []
        let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
            guard let data = data, error == nil else {
                print("Error in request \(error.debugDescription)")
                return
            }
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            guard let jsonData = try? decoder.decode(JsonRoot.self, from: data) else {return}
            for user in jsonData.results {
                users.append(user)
            }
            DispatchQueue.main.async {
                completion(users)
            }
        }
        task.resume()
    }
    
}
