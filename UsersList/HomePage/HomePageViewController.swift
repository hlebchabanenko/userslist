//
//  ViewController.swift
//  UsersList
//
//  Created by mac on 05.03.2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    
    @IBOutlet weak var usersTableView: UITableView!
    
    var randomUser: [Result] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Service.downloadJson { [weak self] (users) in
            self?.randomUser = users
            self?.usersTableView.reloadData()
        }
    }
    
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return randomUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? HomePageTableViewCell {
            
            let user = randomUser[indexPath.row]
            guard let photo = try? user.picture.large.asURL() else { return UITableViewCell() }
            cell.setup(firstName: user.name.first, lastName: user.name.last, phone: user.phone)
            cell.photoImageView.setImageWith(url: photo)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let controller = storyboard?.instantiateViewController(identifier: "EditUserViewController") as? EditUserViewController else {return}
        controller.users = randomUser[usersTableView.indexPathForSelectedRow!.row]
        navigationController?.pushViewController(controller, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
