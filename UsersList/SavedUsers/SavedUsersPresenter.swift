//
//  SavedUsersPresenter.swift
//  UsersList
//
//  Created by mac on 08.03.2021.
//

import UIKit

protocol SavedUsersPresenterOutput: class {
    func didDataUpdate()
}
class SavedUsersPresenter: NSObject {
    
    weak var output: SavedUsersPresenterOutput?
    
    var info = [UserInfo]() {
        didSet {
            output?.didDataUpdate()
        }
    }
}

extension SavedUsersPresenter: SavedUsersViewControllerOutput {
    
    func loadData() {
        guard let result = try? CoreDataManager.shared.viewContext.fetch(UserInfo.fetchRequest()) as? [UserInfo] else { return }
        info = result
    }
    
}
