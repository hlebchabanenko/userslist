//
//  EditUserTableViewCell.swift
//  UsersList
//
//  Created by mac on 06.03.2021.
//

import UIKit

class EditUserTableViewCell: UITableViewCell {
    
    @IBOutlet weak private var keyLabel: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    
    func setup (key: String, value: String) {
        
        keyLabel.text = key
        valueTextField.text = value
    }
}
