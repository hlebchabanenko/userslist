//
//  CurrentUser.swift
//  UsersList
//
//  Created by mac on 06.03.2021.
//

import Foundation
import UIKit
import SwiftyJSON

struct JsonRoot: Codable {
    let results: [Result]
}

struct Result: Codable {
    var name: Name = Name(first: "", last: "")
    var picture: Picture = Picture(medium: "", large: "")
    let phone: String
    let email: String
    
    init(userCoreData: UserInfo) {
        self.name.first = userCoreData.firstName ?? ""
        self.name.last = userCoreData.lastName ?? ""
        self.picture.medium =  ""
        self.phone = userCoreData.phone ?? ""
        self.email = userCoreData.email ?? ""
        
    }
}

struct Name: Codable {
    var first, last: String
    init(first: String, last: String) {
        self.first = first
        self.last = last
    }
}

struct Picture: Codable {
    var medium: String
    var large: String
    init(medium: String, large: String) {
        self.medium = medium
        self.large = large
    }
}
