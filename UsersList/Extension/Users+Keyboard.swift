//
//  Users+Keyboard.swift
//  UsersList
//
//  Created by mac on 06.03.2021.
//

import Foundation
import UIKit

extension UIViewController {
    
    func hideKeyboardWhenTappedaround() {
        let taps = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dissmissKeyboard))
        taps.cancelsTouchesInView = false
        view.addGestureRecognizer(taps)
    }
    
    @objc func dissmissKeyboard () {
        view.endEditing(true)
    }
}
