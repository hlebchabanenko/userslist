import UIKit
import Nuke

typealias SuccessCompletion = (Bool) -> Void

protocol NukeLoadable: class {
    var imageLoadable: UIImage? { get set }
    func setImageWith(url: URL?, useActivity: Bool, placeholder: PlatformImage?,
                      options: ImageLoadingOptions?, completion: SuccessCompletion?) -> ImageTask?
}

extension UIImageView {
    @discardableResult
    func setImageWith(url: URL?, useActivity: Bool = false, placeholder: PlatformImage? = nil,
                      options: ImageLoadingOptions? = nil, completion: SuccessCompletion? = nil) -> ImageTask? {
        guard let url: URL = url else {
            completion?(false)
            return nil
        }

        if useActivity {
          //  showActivityIndicator()
        }

        var loadingOptions: ImageLoadingOptions = options ?? ImageLoadingOptions.shared
        loadingOptions.transition = .fadeIn(duration: 0.25)
        loadingOptions.placeholder = placeholder
        let request: ImageRequest = ImageRequest(url: url)

        let task: ImageTask? = Nuke.loadImage(with: request, options: loadingOptions, into: self, progress: nil) { [weak self] response in
            if useActivity {
                
            }

            if let image = try? response.get().image {
                self?.image = image
                completion?(true)
            } else {
                completion?(false)
            }

        }

        return task
    }
}
