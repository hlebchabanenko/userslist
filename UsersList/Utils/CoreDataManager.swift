//
//  CoreDataManager.swift
//  UsersList
//
//  Created by mac on 07.03.2021.
//

import CoreData

class CoreDataManager {
    
    static let shared = CoreDataManager()
    
    private let modelFileName = "Model"
    
    private init() { }
    
    var viewContext: NSManagedObjectContext { persistentContainer.viewContext }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: modelFileName)
        container.loadPersistentStores(completionHandler: { (_, _) in })
        return container
    }()
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch { }
        }
    }
}
